import React from "react";
import styles from "./Post.module.css";

const Post = (props) => {
  return (
    <div className={styles.item}>
      <img
        src="https://lh3.googleusercontent.com/proxy/RonQxzoeo9qidbtOyKZzop2gC9HjebJU2WaoEwPktSewAeQI38CURcKVYnGPyA6CDUReiIYrEYKMI8-j"
        alt=""
      />
      {props.message}
      <div>
        <span>Like</span> {props.likesCount}
      </div>
    </div>
  );
};
export default Post;
